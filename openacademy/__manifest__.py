{
    'name': "Open Academy v13",

    'summary': """Manage trainings""",

    'author': "jorgescalona",
    'website': "http://www.attakatara.wordpress.com",

    'category': 'Test',
    'version': '12.0.1.0.0',
    'license': 'AGPL-3',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        # 'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'installable': True,
}
